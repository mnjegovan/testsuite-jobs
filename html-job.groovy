def report_Directory = 'reports/*.xml'

def reportResults(directory){
  junit directory
}

node {
  stage("Clone"){
  git "https://gitlab.com/mnjegovan/testsuite.git" 
  }
  
  stage("Test"){
    try{
			sh "mkdir -p reports"
			withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: "gitlab", usernameVariable: 'GITLAB_USER', passwordVariable: 'GITLAB_PW']]) {
       sh "behave --junit --junit-directory reports"
			}
    } catch(err){
      reportResults(report_Directory)
			error "${err}"
		}
  stage("Report"){
    reportResults(report_Directory)
  	}
	}
}
